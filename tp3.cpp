#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <cmath>
#include <string.h>
#include <fstream>

using namespace std;
using namespace cv;

const char *dirExamples = "coil-100";
const char *imageExtension = ".png";

typedef struct {
    string name;
    float distance;
}descriptionImg;

vector<string> getExamples(const char *dir);
int isImageFile(char *f);
void getHistogram(char *file, float *histogrameArray);
float calculateDistanceHistogram(float *his1, float *his2);
void knnProcessing(char *nameImgQuery);
Mat reduceQuantization(char *file);
float calculateBasicMoment(Mat image, int p, int q);
float calculateCentralizedMoment(Mat image, float _x, float _y, int p, int q);
float calculateNormalizedMoment(float centralizedMoment, int i, int j, float centralizedMoment00);
void calculateHuMoment(char* nameImg, float *hu);
double calculateDistanceHuMoment(double *huMoment1, double *huMoment2);

void cal_Hu_moment(char *file, double *hu);
Mat reduction_img_girs(Mat img_original) ;

int scale, nbNeighbour;
double *huMomentImage;
descriptionImg *descriptionImgs;

int main(int argc, char ** argv){
    scale = atoi(argv[2]);
    nbNeighbour = atoi(argv[3]);
    huMomentImage = new double[7];

    char imageUrl[128];
    sprintf(imageUrl, "%s/%s", dirExamples, argv[1]);
    cal_Hu_moment(imageUrl, huMomentImage);
   

    descriptionImgs = new descriptionImg[nbNeighbour];

    for(int i = 0; i < nbNeighbour; i++){
        descriptionImgs[i].name =  "unknown";
        descriptionImgs[i].distance = 100000.0;
    }

   /** char imageUrl[128];
    sprintf(imageUrl, "%s/%s", dirExamples, argv[1]);
    calculateHuMoment(imageUrl, huMomentImage);*/

    knnProcessing(argv[1]);

    cout<<"List neighbour : \n";
    for(int i = 0; i < nbNeighbour; i++){
        cout<<descriptionImgs[i].name<<" "<<descriptionImgs[i].distance<<endl;
    }

    delete descriptionImgs;
    delete huMomentImage;

    return 0;
}

void cal_Hu_moment(char *file, double *hu) {
    Mat img = imread(file, CV_LOAD_IMAGE_COLOR);
    Mat img_gris_reduit = reduction_img_girs(img); 
    Moments moment = moments(img_gris_reduit, false);
    HuMoments(moment, hu);
}

Mat reduction_img_girs(Mat img_original) {
    Mat img_gris = img_original.clone();
    cvtColor(img_original, img_gris, CV_RGB2GRAY);
    Mat img_gris_reduit = img_gris.clone();
    int intervalle = 256/scale;
    for(int y = 0; y < img_gris_reduit.rows; y++) {
        for(int x = 0; x < img_gris_reduit.cols; x++) {
            int pixel = img_gris_reduit.at<uchar>(y,x)/intervalle;
            img_gris_reduit.at<uchar>(y,x) = pixel;

        }
    }
    return img_gris_reduit;
}

Mat reduceQuantization(char *file){
    Mat image = imread(file, CV_LOAD_IMAGE_COLOR);

    for (int i = 0; i < image.size().width; i++){
        for (int j = 0; j < image.size().height; j++){
            Vec3b pixel = image.at<Vec3b>(j, i);

            int grey = ((int)pixel.val[0] + (int)pixel.val[1] + (int)pixel.val[2]) / 3;
            grey = grey * scale / 256;
            image.at<uchar>(j, i) = grey;
        }
    }

    return image;
}

float calculateBasicMoment(Mat image, int p, int q){
    float basicMoment = 0;
    for (int i = 0; i < image.size().width; i++){
        for (int j = 0; j < image.size().height; j++){
            basicMoment += pow(j, p) * pow(i, q) * image.at<uchar>(j, i);
        }
    }
    return basicMoment;
}

float calculateCentralizedMoment(Mat image, float _x, float _y, int p, int q){
    float centralizedMoment = 0;
    for (int i = 0; i < image.size().width; i++){
        for (int j = 0; j < image.size().height; j++){
            centralizedMoment += pow(j - _x, p) * pow(i - _y, q) * image.at<uchar>(j, i);
        }
    }
    return centralizedMoment;
}

float calculateNormalizedMoment(float centralizedMoment, int i, int j, float centralizedMoment00){
    return (centralizedMoment / pow(centralizedMoment00, (1 + (i + j) / 2.0)));
}

void calculateHuMoment(char* nameImg, float *hu){
    Mat img = reduceQuantization(nameImg);

    float basicM10 = calculateBasicMoment(img, 1, 0);
    float basicM01 = calculateBasicMoment(img, 0, 1);
    float basicM00 = calculateBasicMoment(img, 0, 0);

    float _x = basicM10 / basicM00;
    float _y = basicM01 / basicM00;
    float centralizedM[4][4];
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            centralizedM[i][j] = calculateCentralizedMoment(img, _x, _y, i, j);
        }
    }

    float normalizedM[4][4];
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            normalizedM[i][j] = calculateNormalizedMoment(centralizedM[i][j], i, j, centralizedM[0][0]);
        }
    }

    hu[0] = normalizedM[2][0] + normalizedM[0][2];

    hu[1] = pow((normalizedM[2][0] - normalizedM[0][2]),2) + 4 * pow(normalizedM[1][1], 2);

    hu[2] = pow((normalizedM[3][0] - 3 * normalizedM[1][2]),2) + pow((3 * normalizedM[2][1] - normalizedM[0][3]),2);

    hu[3] = pow((normalizedM[3][0] + normalizedM[1][2]),2) + pow((normalizedM[2][1] + normalizedM[0][3]),2);

    hu[4] = (normalizedM[3][0] - 3 * normalizedM[1][2]) * (normalizedM[3][0] + normalizedM[1][2])
            * (pow((normalizedM[3][0] + normalizedM[1][2]), 2) - 3 * pow((normalizedM[2][1] + normalizedM[0][3]), 2))
            + (3 * normalizedM[2][1] - normalizedM[0][3]) * (normalizedM[2][1] + normalizedM[0][3])
            * (3 * pow((normalizedM[3][0] + normalizedM[1][2]), 2) - pow((normalizedM[2][1] + normalizedM[0][3]), 2));

    hu[5] = (normalizedM[2][0] - normalizedM[0][2])
            * (pow((normalizedM[3][0] + normalizedM[1][2]),2) - pow((normalizedM[2][1] + normalizedM[0][3]),2))
            + 4 * normalizedM[1][1] * (normalizedM[3][0] + normalizedM[1][2]) * (normalizedM[2][1] + normalizedM[0][3]);

    hu[6] = (3 * normalizedM[2][1] - normalizedM[0][3]) * (normalizedM[3][0] + normalizedM[1][2])
            * (pow((normalizedM[3][0] + normalizedM[1][2]), 2) - 3 * pow((normalizedM[2][1] + normalizedM[0][3]), 2))
            - (-3 * normalizedM[1][2] + normalizedM[3][0]) * (normalizedM[2][1] + normalizedM[0][3])
            * (3 * pow((normalizedM[3][0] + normalizedM[1][2]), 2) - pow((normalizedM[2][1] + normalizedM[0][3]), 2));
}

vector<string> getExamples(const char *dir){
    DIR *dp;
    struct dirent *ep;
    vector<string> listFile;

    dp = opendir (dir);
    if (dp != NULL){
        while (ep = readdir (dp)){
            if(isImageFile(ep->d_name)){
                listFile.push_back(ep->d_name);
            }
        }
        (void) closedir (dp);
    }else{
        perror ("Couldn't open the directory");
    }
    return listFile;
}

int isImageFile(char *f){
    if(strstr(f, imageExtension)){
        return 1;
    }
    return 0;
}

void knnProcessing(char* nameImgQuery){
    vector<string> listFile;
    listFile = getExamples(dirExamples);
    for (vector<string>::iterator it = listFile.begin() ; it != listFile.end(); ++it){
        char imageUrl[128];
        if(strcmp(nameImgQuery, (*it).c_str()) != 0){
            sprintf(imageUrl, "%s/%s", dirExamples, (*it).c_str());
            double *huMomentNeighbour = new double[7];
            cal_Hu_moment(imageUrl, huMomentNeighbour);

            descriptionImg tmp;
            tmp.name = imageUrl;
            tmp.distance = calculateDistanceHuMoment(huMomentImage, huMomentNeighbour);
            for(int i = 0; i < nbNeighbour; i++){
                if(tmp.distance <= descriptionImgs[i].distance){
                    for(int j = nbNeighbour - 1; j > i; j--){
                        descriptionImgs[j].name = descriptionImgs[j - 1].name;
                        descriptionImgs[j].distance = descriptionImgs[j - 1].distance;
                    }
                    descriptionImgs[i].name = tmp.name;
                    descriptionImgs[i].distance = tmp.distance;
                    break;
                }
            }

            for (int i = 0; i < nbNeighbour; i++)
            {
                cout<<descriptionImgs[i].name<<" "<<descriptionImgs[i].distance<<endl;
            }
            cout<<"==================\n";

            delete huMomentNeighbour;
        }
    }
}

double calculateDistanceHuMoment(double *huMoment1, double *huMoment2){
    double a = 0;
    for(int i = 0; i < 7; i++){
        a += pow((huMoment1[i] - huMoment2[i]), 2);
    }
    return sqrt(a) / 7;
}

